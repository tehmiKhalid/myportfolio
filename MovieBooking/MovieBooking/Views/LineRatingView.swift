
//  LineRatingView.swift


import SwiftUI

struct LineRatingView: View {
    var height: CGFloat = 10
    var value: Double = 0.0
    //The method creates a path that has a gray background color and takes the entire width.
    fileprivate func createPath(with size: CGSize) -> some View {
        return Rectangle()
            .frame(width: size.width, height: self.height, alignment: .leading).cornerRadius(self.height / 2)
            .foregroundColor(Color.gray.opacity(0.5))
    }
    //The  method creates the progress indicator with a beautiful gradient color. This will be animatable reflecting the rating value.
    
    
    fileprivate func createProgress(with size: CGSize) -> some View {
        return Rectangle()
            .fill(LinearGradient(gradient: Gradient(colors: [Color.accent, .red]) , startPoint: .leading, endPoint: .trailing))
            .animation(.easeIn(duration: 1))
            .frame(width: size.width * CGFloat(self.value) / 5, height: self.height, alignment: .leading)
            .cornerRadius(self.height / 2)
    }
    
    var body: some View {
        HStack {
            //We then put everything inside a ZStack with the alignment set to leading for the progress to start on the left rather than the center.
            GeometryReader{ gr in
                ZStack(alignment: .leading) {
                    self.createPath(with: gr.size)
                    self.createProgress(with: gr.size)
                }
            }.frame(height: 15)
            
            Text("\(String(format: "%.1f", self.value))/5")
                .bold()
                .foregroundColor(Color.red)
        }
    }
}

struct LineRatingView_Previews: PreviewProvider {
    static var previews: some View {
        LineRatingView()
    }
}
