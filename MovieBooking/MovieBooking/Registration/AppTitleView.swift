//
//  AppTitleView.swift
//  FirebaseLogin


import SwiftUI

struct AppTitleView: View {
    
    var Title: String
    var body: some View {
        
        VStack {
            VStack(alignment: .leading) {
                   
                
                
                Text(" ").font(.system(size: 24)).fontWeight(.ultraLight).frame(minWidth: 0, maxWidth: .infinity, alignment: .topLeading).foregroundColor(Color.white)
                
                Text(Title).bold() .font(.system(size: 60)).fontWeight(.semibold).frame(minWidth: 0, maxWidth: .infinity, alignment: .topLeading).foregroundColor(Color.white)
                
            
                
            }
        }.padding(.top, 30).padding(.leading, 10).background(Color.black).shadow(radius:21)
    }
    
}
struct AppTitleView_Previews: PreviewProvider {
    static var previews: some View {
        AppTitleView(Title: "Example")
    }
}
