//
//  WelcomeView.swift
//  FirebaseLogin


import SwiftUI
import Firebase

public var screenWidth: CGFloat {
     return UIScreen.main.bounds.width
 }
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

struct WelcomeView: View {
  
    let signOut :Bool?
       @State var signUpIsPresent: Bool = false
       @State var signInIsPresent: Bool = false
       @State var selection: Int? = nil
       @State var viewState = CGSize.zero
       @State var MainviewState =  CGSize.zero
       @State var pushActive = false
    @State var pushActive2 = false
    
       var body: some View {
        
   
            NavigationView{
            VStack {
                AppTitleView(Title: "MovieShowie")
                        Spacer()
                VStack(spacing:10) {
                            
                            NavigationLink(destination: SignUpView(), isActive: self.$pushActive) {
                              Text("")
                            }.hidden()

                            
                            NavigationLink(destination:  SignInView(onDismiss:{
                                                                 
                                                             self.viewState = CGSize(width: screenWidth, height: 0)
                                                             self.MainviewState = CGSize(width: 0, height: 0)
                                                                 
                                }), isActive: self.$pushActive2) {
                                                         Text("")
                            }.hidden()
                            
                            
                            Button(action: {

                                self.pushActive = true
                                

                            }
                            ) {
                                Text("        Sign Up       ").foregroundColor(Color.white).bold().frame(width: 225, height: 50) .font(.largeTitle).background(Color.gray).cornerRadius(15)
                            }

                            Button(action: {

                                self.pushActive2 = true
                                

                            }
                            ) {
                                Text("         Sign In        ").foregroundColor(Color.white).bold().frame(width: 225, height: 50) .font(.largeTitle).background(Color.gray).cornerRadius(15)
                            }
                                
                        
                            
                }.background(
                Image("rf")
                    .resizable()
                    .frame(width: 600, height: 750).opacity(100).blur(radius: 3))
                        Spacer()
                              
                }.edgesIgnoringSafeArea(.top).edgesIgnoringSafeArea(.bottom)
                .offset(x:self.viewState.width).animation(.spring())
            
            
            }
            

        
       
   }
    
}



struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView(signOut:false)
    }
}

