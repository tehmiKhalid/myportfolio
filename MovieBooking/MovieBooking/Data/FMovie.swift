//
//  Movie.swift
//Details of all Trailer movies

import SwiftUI
//Identifiable: these type can be identified uniquely & it must have id
struct FMovie: Hashable, Codable, Identifiable {
    var id: Int
    var thumbnail: String
   
    var title: String
    var description: String
    var castdescription: String
    var trailerLink: String
    var catagory: Catagory
    var isFeaturedMovie: Bool
    var cast:[String]
    
    enum Catagory: String, CaseIterable, Codable, Hashable {
        case marvel = "Marvel"
        case dc = "DC"
        case actionAdventure = "Action and adventure"
    }
}
