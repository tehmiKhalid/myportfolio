

import Foundation

struct CastModel: Hashable, Decodable {
    //Decodable : decode itself from an external representation,JSON data is decoded into a format that Swift understands
    var name: String
    var rating: Int
    var image_path: String
    var objectID: String
    var description:String
}


