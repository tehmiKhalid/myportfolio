//
//  MovieDetailView.swift


import SwiftUI

struct SingleMovieView<T: Movie>: View {
    //Generic Type : T (unknown type)
    var movie: T
    
    var body: some View {
        ScrollView(showsIndicators: false) {
                    VStack(alignment: .leading) {
                        createPosterImage()
                        MovieDetailView(movie: self.movie)
                    }
                }.edgesIgnoringSafeArea(.top)
        //If this view does not accept the size proposed for it, it is positioned in the center of the proposed area.
        }
    
    // fileprivate: provide access to this function anywhere in this file
    //This function renders the poster image for the movie
       fileprivate func createPosterImage() -> some View {
           return Image(uiImage: UIImage(named: "\(movie.image).jpg") ?? UIImage() ).resizable()
           .aspectRatio(contentMode: .fit)
       }
}

struct SingleMovieView_Previews: PreviewProvider {
    static var previews: some View {
        SingleMovieView<Popular>(movie: Popular.default)
    }
}
