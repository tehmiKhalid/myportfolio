
//  MovieListView.swift

import SwiftUI

struct MovieListView<T: Movie>: View {
  //Generic Struct because we want all movies type
    var movies: [T]
    var section: HomeSection
    
    var body: some View {
        NavigationView {
            List(0..<movies.count) { i in
                    MovieListRow<T>(movie: self.movies[i])
            }.navigationBarTitle(section.rawValue)
        }
    }
}

struct MovieListView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListView<Popular>(movies: [], section: .Trending)
    }
}
