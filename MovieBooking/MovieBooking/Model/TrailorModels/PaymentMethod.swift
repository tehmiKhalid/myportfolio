
//  PaymentMethod.swift



import Foundation


enum PaymentMethod: String, CaseIterable{
    case MasterCard,Visa, Paypal
}
