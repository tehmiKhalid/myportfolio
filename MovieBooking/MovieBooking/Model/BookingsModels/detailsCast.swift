//
//  detailsCast.swift
//  Projec_iOS
//
//  Created by Taha Siddiqui on 08/02/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import SwiftUI

struct detailsCast: View {
    var actorName: String? = ""
    var objActor:CastModel
    
    var body: some View {
        
        VStack {
            HStack {
                ScrollView(showsIndicators: false) {
                    
                    VStack(alignment: .leading ,spacing: 5) {
                       // ForEach(0 ..< 3) {
                            Image(objActor.name)
                                           .resizable()
                                           .aspectRatio(contentMode: .fit)
                            Text(objActor.name)
                                           .font(.system(size: 35, weight: .black, design: .rounded))
                                           .padding(.horizontal)
                        Text(objActor.description)
                                       .font(.body)
                                       .padding()
                              // }
                    
                    }.navigationBarTitle(Text("Cast").bold())
                }

        }
    }
        
}
}

struct detailsCast_Previews: PreviewProvider {
    static var previews: some View {
        detailsCast(actorName: "", objActor: CastModel(name: "", rating: 0, image_path: "", objectID: "",description: ""))
    }
}

