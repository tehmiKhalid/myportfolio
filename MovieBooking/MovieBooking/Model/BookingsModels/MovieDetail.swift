

import SwiftUI

struct MovieDetail : View {
    var movie: FMovie
    var castdetails = castData; // let castData:[CastModel] = load("actors.json"),Have CastModel Array , in movieData
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            ZStack(alignment: .bottom) {
                Image(movie.thumbnail)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Rectangle()
                    .frame(height: 80)
                    .opacity(0.25)
                    .blur(radius: 10)
                HStack {
                    VStack(alignment: .leading, spacing: 8) {
                        Text(movie.title)
                            .foregroundColor(.white)
                            .bold()
                            .font(.largeTitle)
                    }
                    .padding(.leading)
                    .padding(.bottom)
                    Spacer()
                }
            }
            VStack(alignment: .center,spacing: 10) {
                Text(movie.description)
                    .foregroundColor(.primary)
                    .font(.body)
                    .lineSpacing(14)
                
                WatchButton(movie: movie)
                    .padding()
                Cast(movie: movie).padding()
            }.padding(.all)
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0,maxHeight: .infinity, alignment: .topLeading)
        }
        .edgesIgnoringSafeArea(.top)
        .navigationBarHidden(true)
    }
}

struct MovieDetail_Preview: PreviewProvider {
    static var previews: some View {
        MovieDetail(movie: moviesData.first!)
    }
}

struct WatchButton: View {
    let movie: FMovie
    @State var showingDetail = false
      @State var locationDetail = false
    var body: some View {
        HStack{
        VStack {
            Button(action: {
                self.showingDetail.toggle()
            }) {
                Text("Watch Trailer")
                    .frame(width: 160, height: 50,alignment: .center)
                    .foregroundColor(.white)
                    .font(.headline)
                    .background(Color.blue)
                    .cornerRadius(10)
                
            } .sheet(isPresented: $showingDetail){
                TrailerView(movie: self.movie)
            }
        }
        
        ////Trailer button/ end///
        VStack {
            
                  Button(action: {
                      self.locationDetail.toggle()
                  }) {
                      Text("Location")
                          .frame(width: 160, height: 50,alignment: .center)
                          .foregroundColor(.white)
                          .font(.headline)
                          .background(Color.yellow)
                          .cornerRadius(10)
                      
                  } .sheet(isPresented: $locationDetail){
                      Location(movie: self.movie)
                  }
              }
        }
        
        
        
        
    }
}

struct WatchButton_Preview: PreviewProvider {
    static var previews: some View {
        WatchButton(movie: moviesData.first!)
    }
}


struct Cast: View {
    let movie: FMovie
    @State var scale: CGFloat = 5.0
    @State var selection: Int? = nil
    
     var featuredMovies = moviesData.filter({$0.isFeaturedMovie == true})
    
    var body: some View {
      /**NavigationLink(destination: MovieDetail(movie: featuredMovies[0])) {
          PageView(featuredMovies.map { FeaturedMovieView(movie: $0) })
              .frame(height: 225)
      }*/
        HStack {
            VStack {
                
                Text(movie.cast[0])
                    HStack {
                        NavigationLink(destination: detailsCast(actorName: movie.cast[0],objActor: castData.filter({$0.name == movie.cast[0]}).first!), tag: 1, selection: $selection) {
                        Button(action: {
                            self.selection = 1;
                        }){
                            Image(movie.cast[0]) .renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original)).resizable().frame(width:100.0,height: 90.0).clipShape(Circle()).shadow(radius: 4)
                        }
                    }
                 
                }
                
            }
            
            VStack {

                  Text(movie.cast[1])
                    HStack {
                        NavigationLink(destination: detailsCast(actorName: movie.cast[0],objActor: castData.filter({$0.name == movie.cast[1]}).first!), tag: 2, selection: $selection){
                         
                        Button(action: {
                            self.selection = 2;
                        }){
                            Image(movie.cast[1]) .renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original)).resizable().frame(width:100.0,height: 90.0).clipShape(Circle()).shadow(radius: 4)
                        }
                    }
                }
            }
            
            VStack {
                  Text(movie.cast[2])
                    HStack {
                        NavigationLink(destination: detailsCast(actorName: movie.cast[2],objActor: castData.filter({$0.name == movie.cast[2]}).first!), tag: 3, selection: $selection){
                        Button(action: {
                            self.selection = 3;
                        }){
                            Image(movie.cast[2]) .renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original)).resizable().frame(width:100.0,height: 90.0).clipShape(Circle()).shadow(radius: 4)
                        }
                    }
                }
                                    
            }

        }

    }
}

struct Cast_Previews: PreviewProvider {
    static var previews: some View {
        Cast(movie: moviesData.first!)
    }
}

