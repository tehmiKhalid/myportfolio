//
//  CircleImage.swift
//  Projec_iOS
//
//  Created by macbook on 09/02/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    let movie: FMovie
    
    var body: some View {
        Image(movie.thumbnail).clipShape(Circle())
            .overlay(
            Circle().stroke(Color.white, lineWidth: 4))
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage(movie: moviesData.first!)
    }
}
