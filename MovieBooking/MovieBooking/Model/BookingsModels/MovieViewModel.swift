
//  MovieViewModel.swift
//HomeSection: used as keys to retrieve specific sections from the model.

import SwiftUI

enum HomeSection: String, CaseIterable {
    //CaseIterable : Collection of all type's cases
    case Trending
    case Popular
    case Upcoming
    case Actors
}


class MovieViewModel: ObservableObject {
    
    @Published var allItems: [HomeSection:[Codable]] = [:]
    //ObservableObject: Property wrapper , to keep the user uptoDate
    //allitems : Dictionay, [key -> HomeSection , value ->  Data]
    //Published : Automatically announce changes to properties , it reinvokes the body to sync with data
    //Codable is a type alias for the Encodable and Decodable protocols.
    
    init() {
        //initializing the properties
        getAll()
    }
    
    private func getAll(){
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            
            do {
                let url = URL(fileURLWithPath: path)
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                //mappedIfSafe : memory Mapping , data mapped into virtual memory, if possible and safe , create pointer in disk
                let decoder = JSONDecoder()
                let result = try decoder.decode(MovieBundle.self, from: data)
                
                allItems = [HomeSection.Trending: result.trending,
                            HomeSection.Popular: result.popular,
                            HomeSection.Upcoming: result.upcoming,
                            HomeSection.Actors: result.actors]
                
            } catch let e{
                print(e)
            }
        }
    }
}

