//
//  Location.swift
//  Projec_iOS
//
//  Created by macbook on 09/02/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import SwiftUI

struct Location: View {
    
    let movie: FMovie
    
    
    
    var body: some View {
          VStack {
              MapView()
                  .frame(height: 300)

            CircleImage(movie: moviesData.first!)
                  .offset(y: -130)
                  .padding(.bottom, -130)

              VStack(alignment: .center) {
                  Text(movie.title)
                    .font(.title)
                
                  HStack(alignment: .top) {
                      Text(movie.description)
                          .font(.subheadline)
                      
                      
                  }
              }
              .padding()
          }
      }
}

struct Location_Previews: PreviewProvider {
    static var previews: some View {
        Location(movie: moviesData.first!)
    }
}

