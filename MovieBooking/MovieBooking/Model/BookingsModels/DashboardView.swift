//
//  DashboardView.swift
//  Projec_iOS
//
//  Created by macbook on 04/02/2020.
//  Copyright © 2020 macbook. All rights reserved.
//



import SwiftUI
import Firebase

struct DashboardView: View {
    var body: some View {
        
        TabView {
            HomeView()
                .tabItem {
                    Image(systemName: "square.grid.2x2.fill")
                    Text("Home")
            } .tag(0)
           
            MovieStoreApp()
                .tabItem {
                    Image(systemName: "tv.fill")
                    Text("Movies")
            } .tag(1)
            
            
            
        }
        
        
    }
    
}

struct DashboardView_Preview: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}

